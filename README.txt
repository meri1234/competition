CONTENTS OF THIS FILE
--------------------

* Introduction
* Requirements

INTRODUCTION
------------

The Competition module provides a form where users can enter a competition.
For this purpose is using Competitor custom entity with all the required fields.
To access the 'Enter Competition' form you can add the Custom Competition Block
somewhere in the block layout or just go to /enter-competition url.
For exporting list of all competitors use 'Download Competitors' button
located on /admin/structure/competitor.

REQUIREMENTS
------------

This module requires phpoffice/phpspreadsheet library,
it's best to install it with composer before installing this module.
Also consider checking php configuration because post_max_size and upload_max_filesize should be set to 500M.
