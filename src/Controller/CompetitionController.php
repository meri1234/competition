<?php

namespace Drupal\competition\Controller;

use Drupal\competition\Entity\Competitor;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityFormBuilder;

/**
* A competition controller.
*/
class CompetitionController extends ControllerBase {

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Entity\EntityFormBuilder
   */
  protected $formBuilder;

  /**
   * ModalFormContactController constructor.
   *
   * @param \Drupal\Core\Entity\EntityFormBuilder $form_builder
   *   The form builder.
   */
  public function __construct(EntityFormBuilder $form_builder) {
    $this->formBuilder = $form_builder;
  }

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.form_builder')
    );
  }

  /**
   * Returns add competitor form.
   */
  public function content() {
    $user_form = $this->formBuilder->getForm(Competitor::create());
    $build['form'] = $user_form;
    return $build;
  }

}
