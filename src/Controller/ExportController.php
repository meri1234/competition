<?php

namespace Drupal\competition\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Entity\EntityTypeManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ExportController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Entity type manager.
   *
   * @var EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * ExportController constructor.
   */
  public function __construct(EntityTypeManager $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Export competitors in xls file.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The generated xls file.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \PhpOffice\PhpSpreadsheet\Exception
   * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
   */
  public function export() {
    $response = new Response();
    $response->headers->set('Pragma', 'no-cache');
    $response->headers->set('Expires', '0');
    $response->headers->set('Content-Type', 'application/vnd.ms-excel');
    $response->headers->set('Content-Disposition', 'attachment; filename=competitors.xlsx');

    $spreadsheet = new Spreadsheet();

    // Get the active sheet.
    $spreadsheet->setActiveSheetIndex(0);
    $worksheet = $spreadsheet->getActiveSheet();

    //Set style Title
    $styleArrayTitle = array(
      'font' => array(
        'bold' => true,
        'color' => array('rgb' => '161617'),
        'size' => 12,
        'name' => 'Verdana'
      ));

    $worksheet->getCell('A1')->setValue('Competitors');
    $worksheet->getStyle('A1')->applyFromArray($styleArrayTitle);


    //Set Background
    $worksheet->getStyle('A3:G3')
      ->getFill()
      ->setFillType(Fill::FILL_SOLID)
      ->getStartColor()
      ->setARGB('085efd');

    //Set style Head
    $styleArrayHead = array(
      'font' => array(
        'bold' => true,
        'color' => array('rgb' => 'ffffff'),
      ));

    $worksheet->getCell('A3')->setValue('ID');
    $worksheet->getCell('B3')->setValue('First Name');
    $worksheet->getCell('C3')->setValue('Last Name');
    $worksheet->getCell('D3')->setValue('E-Mail');
    $worksheet->getCell('E3')->setValue('Gender');
    $worksheet->getCell('F3')->setValue('ZIP');
    $worksheet->getCell('G3')->setValue('Video');

    $worksheet->getStyle('A3:G3')->applyFromArray($styleArrayHead);

    $competitors = $this->entityTypeManager
      ->getStorage('competitor')
      ->loadMultiple();

    $competitors = array_values($competitors);

    foreach ($competitors as $key => $competitor) {
      $worksheet->setCellValue('A' . ($key + 5), $competitor->id());
      $worksheet->setCellValue('B' . ($key + 5), $competitor->get('first_name')->getValue()[0]['value']);
      $worksheet->setCellValue('C' . ($key + 5), $competitor->get('last_name')->getValue()[0]['value']);
      $worksheet->setCellValue('D' . ($key + 5), $competitor->get('email')->getValue()[0]['value']);
      $worksheet->setCellValue('E' . ($key + 5), $competitor->get('gender')->getValue()[0]['value']);
      $worksheet->setCellValue('F' . ($key + 5), $competitor->get('zip')->getValue()[0]['value']);
      $file = $competitor->get('video')->referencedEntities()[0];
      $url = $file->createFileUrl(FALSE);
      $worksheet->setCellValue('G' . ($key + 5), $url);
      $spreadsheet->getActiveSheet()->getCell('G' . ($key + 5))->getHyperlink()->setUrl($url);
    }

    // Get the writer and export in memory.
    $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
    ob_start();
    $writer->save('php://output');
    $content = ob_get_clean();

    // Memory cleanup.
    $spreadsheet->disconnectWorksheets();
    unset($spreadsheet);

    $response->setContent($content);
    return $response;
  }
}
