<?php

namespace Drupal\competition;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Competitor entities.
 *
 * @ingroup competition
 */
class CompetitorListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Competitor ID');
    $header['email'] = $this->t('Email');
    $header['firstname'] = $this->t('First Name');
    $header['lastname'] = $this->t('Last Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\competition\Entity\Competitor $entity */
    $row['id'] = $entity->id();
    $row['email'] = $entity->get('email')->getValue()[0]['value'];
    $row['firstname'] = $entity->get('first_name')->getValue()[0]['value'];
    $row['lastname'] = $entity->get('last_name')->getValue()[0]['value'];
    return $row + parent::buildRow($entity);
  }

}
