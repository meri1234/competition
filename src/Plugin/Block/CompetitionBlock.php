<?php

namespace Drupal\competition\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Render\MetadataBubblingUrlGenerator;

/**
 * Provides a 'Competition' Block.
 *
 * @Block(
 *   id = "competition_block",
 *   admin_label = @Translation("Competition"),
 *   category = @Translation("Custom"),
 * )
 */
class CompetitionBlock extends BlockBase implements ContainerFactoryPluginInterface
{

  /**
   * @var MetadataBubblingUrlGenerator $urlGenerator
   */
  protected $urlGenerator;

  /**
   * @param ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('url_generator')
    );
  }

  /**
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param MetadataBubblingUrlGenerator $urlGenerator
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MetadataBubblingUrlGenerator $urlGenerator) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->urlGenerator = $urlGenerator;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $url = Url::fromRoute('competition.enter_competition');
    $link = [
      '#type' => 'link',
      '#url' => $url,
      '#title' => $this->t('Enter Competition'),
    ];
    return $link;
  }

}
