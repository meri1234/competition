<?php

namespace Drupal\competition\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Competitor entities.
 *
 * @ingroup competition
 */
class CompetitorDeleteForm extends ContentEntityDeleteForm {


}
