<?php

namespace Drupal\competition\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Competitor entity.
 *
 * @ingroup competition
 *
 * @ContentEntityType(
 *   id = "competitor",
 *   label = @Translation("Competitor"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\competition\CompetitorListBuilder",
 *     "views_data" = "Drupal\competition\Entity\CompetitorViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\competition\Form\CompetitorForm",
 *       "add" = "Drupal\competition\Form\CompetitorForm",
 *       "edit" = "Drupal\competition\Form\CompetitorForm",
 *       "delete" = "Drupal\competition\Form\CompetitorDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\competition\CompetitorHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\competition\CompetitorAccessControlHandler",
 *   },
 *   base_table = "competitor",
 *   translatable = FALSE,
 *   admin_permission = "administer competitor entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/competitor/{competitor}",
 *     "add-form" = "/admin/structure/competitor/add",
 *     "edit-form" = "/admin/structure/competitor/{competitor}/edit",
 *     "delete-form" = "/admin/structure/competitor/{competitor}/delete",
 *     "collection" = "/admin/structure/competitor",
 *   },
 *   field_ui_base_route = "competitor.settings"
 * )
 */
class Competitor extends ContentEntityBase implements CompetitorInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['email'] = BaseFieldDefinition::create('email')
      ->setLabel(t('E-Mail'))
      ->setDescription(t('The email of the participant.'))
      ->setDefaultValue('')
      ->setRequired(TRUE)
      ->addConstraint('UserMailUnique')
      ->addConstraint('UserMailRequired')
      ->addConstraint('ProtectedUserField')
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['gender'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Geschlecht'))
      ->setSettings([
        'allowed_values' => [
          'weiblich' => 'Weiblich',
          'mannlich' => 'Männlich',
          'divers' => 'Divers',
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'visible',
        'type' => 'list_default',
        'weight' => 1,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setRequired(TRUE);

    $fields['first_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Vorname'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['last_name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Nachname'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 3,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['zip'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('PLZ'))
      ->setRequired(true)
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'weight' => 4,
      ))
      ->setDisplayOptions('form', array(
        'weight' => 4,
      ))
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true);

    $fields['video'] = BaseFieldDefinition::create('file')
      ->setLabel(t('Video hochladen'))
      ->setDescription(t('The Video of the contestant.'))
      ->setRequired(true)
      ->setSettings([
        'uri_scheme' => 'private',
        'file_directory' => 'competition_videos',
        'file_extensions' => 'mp4',
        'file_validate_size' => '500M'
      ])
      ->setDisplayOptions('view', array(
        'label' => 'above',
        'type' => 'file_video',
        'weight' => 5,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'file',
        'weight' => 5,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
