<?php

namespace Drupal\competition\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Competitor entities.
 *
 * @ingroup competition
 */
interface CompetitorInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Competitor name.
   *
   * @return string
   *   Name of the Competitor.
   */
  public function getName();

  /**
   * Sets the Competitor name.
   *
   * @param string $name
   *   The Competitor name.
   *
   * @return \Drupal\competition\Entity\CompetitorInterface
   *   The called Competitor entity.
   */
  public function setName($name);

  /**
   * Gets the Competitor creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Competitor.
   */
  public function getCreatedTime();

  /**
   * Sets the Competitor creation timestamp.
   *
   * @param int $timestamp
   *   The Competitor creation timestamp.
   *
   * @return \Drupal\competition\Entity\CompetitorInterface
   *   The called Competitor entity.
   */
  public function setCreatedTime($timestamp);

}
